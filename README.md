# Final Project - Data Science

##  SCC5871 Ciência de dados
### ¨Analyzing the perception of security using images of streets¨

Authors:
* Alexis J. Vargas (Usp Number: 11939710)
* Karelia A. Vilca (Usp Number: 11939727)

Semester 1, Year: 2020

## Folders and files:
* [Data Processing Notebook](DataProcessing.ipynb) Contains the notebook with explained code of preprocessing, data analysis, classification and visualization.
* [Machine Learning Advance](/ML-Advance) Contains preprocessing code and first classification experiments.
* [Visualization web](/WEB) Contains two-dimensional plotting of data on an interactive page (https://kareliavs.gitlab.io/final-project-data-science/WEB/).
* [Data](/Data) Contains the analyzed images and the original data, also some backup files.
